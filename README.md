
##  Dataset Information

#### Dataset Information
| Split           | Download | # Keyframes | # Clips | # Videos | Storage|
|-----------------|----------|-----------------|-----------|----------------|--------------|
| Training(Full) | link (506 MB) | 35,216,177 | 1,054,970 | 137,330  | ~441 GB  |
| Training (Aesthetic)    | link (73 MB)  | 5,840,585     | 135,606      | 59,974  | ~56 GB |
| Training (Small)         | link (51 MB)  | 3,518,989     | 105,538      | 65,151  | ~45 GB |
| Validation  | link (27 MB)  | 1,886,109 | 56,360 | 42,316 | ~25 GB |
|  Testing  | link (27 MB) | 1,873,480   | 56,436  | 42,131 | ~25 GB |

## Preperation

Please follow the instructions below to prepare the complete dataset.

#### 1. Setup environment

```bash
git clone https://gitlab.com/dataset4320423/avd_v1.0_pipelines.git
cd avd_v1.0_pipelines

conda create -n animevid -y
conda activate animevid

pip install -r requirement.txt
```

#### 2. Download Dataset

1. Download the parquet files from our [huggingface page](https://huggingface.co/datasets/AnimeCLIP/avd-v1.0-safe).

2. Download the parquet files from the following links and put them into `./download/parquet` folder.

3. Run `./download/download.py` to download the videos, files will be saved in `./download/download` by default.
   
#### 3. Split Videos
Our parquet files contains clip boundary information so users don't need to do the boudary detection again. Run the code to split videos into smaller clips:
```bash
cd prepare_dataset
python split_video.py
```

#### 4. Extract Keyframes
And remove the repetitive frames:
```bash
cd prepare_dataset
python detect_keyframes.py
```

#### 5. Good-To-Go !
At this time, you should have the dataset ready for your research. Enjoy!
